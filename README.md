A simple test for deploying Rust code to Heroku, with a Redis back end.

Play the "game" at
[fart-counter.herokuapp.com](https://fart-counter.herokuapp.com).

## Heroku setup

```bash
# Install command line client
sudo snap install --classic heroku
heroku login

# Link this git project to the Heroku project
heroku git:remote -a fart-counter

# Set up for Rust development
heroku buildpacks:set https://github.com/emk/heroku-buildpack-rust.git

# Add a Redis addon (required my credit card info on Heroku)
heroku addons:create heroku-redis:hobby-dev
```

Heroku-specific instructions on how to run the app are in [Procfile](Procfile).

Heroku sets the `PORT` (what port to listen on) and
`REDIS_URL` (how to access Redis) environment variables when it runs the app.

## To deploy a new version

```bash
git push heroku main
```

## To monitor app and build logs

```bash
heroku logs --tail
```
