use actix_web::{get, web, App, HttpResponse, HttpServer, Responder};
use redis::Commands;
use std::env;

struct AppState {
    redis_url: String,
}

#[get("/")]
async fn fart(data: web::Data<AppState>) -> impl Responder {
    let client = redis::Client::open(data.redis_url.clone())
        .expect("Failed to open REDIS_URL");
    let mut con = client
        .get_connection()
        .expect("Failed to connect to Redis.");

    let mut count: u64 = con.get("farts").unwrap_or(0);
    count += 1;
    let _ = con.set::<&str, u64, u64>("farts", count);

    HttpResponse::Ok().body(format!(
        "<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'/>
    </head>
    <body>
    <p>Fart.</p>
    <p><button onclick='location.reload();'>Re-fart</button></p>
    <p>Total farts so far: {}</p>
    <address>Source code:
        <a href='https://gitlab.com/andybalaam/fart-counter'
        >gitlab.com/andybalaam/fart-counter</a>
    </address>
    </body>
</html>",
        count
    ))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let port = env::var("PORT")
        .unwrap_or_else(|_| "3000".to_string())
        .parse()
        .expect("PORT must be a number");

    let redis_url = env::var("REDIS_URL")
        .expect("Environment variable REDIS_URL must be set.");

    HttpServer::new(move || {
        App::new()
            .data(AppState {
                redis_url: redis_url.clone(),
            })
            .service(fart)
    })
    .bind(("0.0.0.0", port))
    .expect("Unable to bind!")
    .run()
    .await
}
